from django.db import models
from .user import User

class Mesa(models.Model):
    id_Mesa = models.AutoField(primary_key = True)
    Usuario = models.ForeignKey(User, related_name= 'mesa', on_delete=models.CASCADE)
    Numero_Sillas = models.IntegerField(default=1)
    Ubicacion_Mesas = [
        ('FRD', 'Frontal Derecha'),
        ('FRI', 'Frontal Izquierda'),
        ('POD', 'Posterior Derecha'),
        ('POI', 'Posterior Izquierda'),
    ]
    Ubicacion = models.CharField(
        max_length=20,
        choices = Ubicacion_Mesas,
        default='POI'
    )
    TiempoReservaDisponible = [
        ('LDR', 'Larga Duración'),
        ('MDR', 'Media Duración'),
        ('CDR', 'Corta Duración'),
    ]
    Tiempo_Reserva = models.CharField(
        max_length=20,
        choices=TiempoReservaDisponible,
        default='CDR'
    )
    Disponibilidad = models.BooleanField(default = True)