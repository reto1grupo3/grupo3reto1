from django.db import models

# Create your models here.
class Reserva(models.Model):
    Estado = [ 
    ("Disponible", "Disponible"),
    ("En Uso", "En Uso"),
    ("Cancelado", "Cancelado"),
    ]
    idReserva = models.CharField(max_length = 150)
    horaInicio = models.CharField(max_length = 150)
    horaFin = models.CharField(max_length = 150)
    fecha = models.DateTimeField(auto_now_add=True)
    Estado = models.CharField(
        max_length = 20,
        choices = Estado,
        default = 'Disponible'
        )
    def __str__(self) -> str:
        return self.idReserva