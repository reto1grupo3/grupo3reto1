from django.contrib import admin
from .models.mesa import Mesa
from .models.reserva import Reserva
from .models.user import User
# Register your models here.

admin.site.register(Mesa)
admin.site.register(Reserva)
admin.site.register(User)
