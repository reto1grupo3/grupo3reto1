from rest_framework import serializers
from reservarApp.models.user import User
from reservarApp.models.mesa import Mesa
from reservarApp.serializers.mesaSerializer import MesaSerializer

class UserSerializer(serializers.ModelSerializer):

    account = MesaSerializer()
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'name', 'email', 'account', 'phone']

    def create(self, validated_data):
        mesaData = validated_data.pop('mesa')
        userInstance = User.objects.create(**validated_data)
        Mesa.objects.create(user=userInstance, **mesaData)
        return userInstance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        account = Mesa.objects.get(user=obj.id)
        return {            
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'account': {                
                'id': account.id,
                'balance': account.balance,
                'lastChangeDate': account.lastChangeDate,
                'isActive': account.isActive
            }
        }

