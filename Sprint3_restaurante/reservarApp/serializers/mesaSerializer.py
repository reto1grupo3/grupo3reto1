from reservarApp.models.mesa import Mesa
from rest_framework import serializers

class MesaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mesa
        fields = ['Numero_Sillas', 'Ubicacion','Tiempo_Reserva', 'Disponibilidad']